const http = require('http')

let mockDB = [
	{
		"firstName": "Mary Jane",
		"lastName": "Dela Cruz",
		"mobileNo": "09123456789",
		"email": "mjdelacruz@mail.com",
		"password": 123
	},
	{
		"firstName": "John",
		"lastName": "Doe",
		"mobileNo": "09123456789",
		"email": "jdoe@mail.com",
		"password": 123
	}
]

http.createServer((request,response) => {

if (request.url == '/profile' && request.method == "GET"){
	response.writeHead(200,{'Content-Type':'application/json'})
	response.write(JSON.stringify(mockDB))
	response.end()


}




if(request.url == '/profile' && request.method == "POST"){
	
	// initialized new DB as empty string
	let requestBody = ''


	request.on('data',(data) => {
		requestBody += data
	})


	request.on('end',() => {
		// mockDB is an object
		// console.log(typeof mockDB)
		// console.log(mockDB)

		console.log('initial ===========')
		// requestBody is considered as string
		console.log(typeof requestBody)
		console.log(requestBody)



		requestBody = JSON.parse(requestBody)
		console.log('after parse ===========')
		// requestBody is now an object
		console.log(typeof requestBody)
		console.log(requestBody)

		// cloning the content of requestbody for pushing in the DB
		let newProfile = {
				"firstName": requestBody.firstName,
				"lastName":  requestBody.lastName,
				"mobileNo":  requestBody.mobileNo,
				"email":  requestBody.email,
				"password":  requestBody.password
			}


		console.log('newProfile FOR PUSHING ===========')
		console.log(typeof newProfile)
		console.log(newProfile)



		mockDB.push(newProfile)	
		console.log('MOCKdb with newprofie ===========')
		console.log(mockDB)



		response.writeHead(200,{'Content-Type':'application/json'})
		response.write(JSON.stringify(newProfile))
		response.end()
	}
	)

}


}).listen(4000)


console.log("Server is connected.")