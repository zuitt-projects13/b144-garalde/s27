const http = require('http')

let directory = [
	{
		"name": "Brandon",
		"email":"brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email":"jobert@mail.com"
	}

]



http.createServer((request,response) => { 

	if(request.url == '/users' && request.method == 'GET'){

		response.writeHead(200,{'Content-Type':'application/json'})
		response.write(JSON.stringify(directory))
		response.end()
	// }1. the Data is received from the client and is processed in the data stream
	}

	if(request.url == '/users' && request.method == "POST"){
		// declare and initialize requestBody variable to an empt string
		// data sent in the request will be saved in requestBody
		let requestBody = ''

		// stream - is a sequesnce of data (how the data will be processed)
		// 1. the Data is received from the client and is processed in the data stream
		request.on('data', function(data){			requestBody += data;		})

		request.on('end', function(){ 
			console.log(typeof requestBody)

			requestBody = JSON.parse(requestBody)

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			directory.push(newUser)
			console.log(directory)

			response.writeHead(200,{'Content-Type':'application/json'})
			response.write(JSON.stringify(newUser))
			response.end()


		})



	}


}).listen(4000)

console.log('Server running at localhost:4000')