const http = require('http');

const port = 4000;

const server = http.createServer((request,response) => {
	// res.end('hello')
	// the HTTP method of the incoming request can be accessed via the 'method' property of the 'request' parameter
	if(request.url == '/items' && request.method == "GET"){
		response.writeHead(200,{'Content-Type':'text/plain'})	
		response.end('Data retrieve from the database')

	}

	if(request.url == '/items' && request.method == "POST"){
		response.writeHead(200,{'Content-Type': 'text/plain'})
		response.end('Data to be sent to the database')
	}

	if(request.url == '/updateItem' && request.method == "PUT"){
		response.writeHead(200,{'Content-Type': 'text/plain'})
		response.end('Update our resources.')
	}

	if(request.url == '/delete' && request.method == "DELETE"){
		response.writeHead(200,{'Content-Type': 'text/plain'})
		response.end('Delete our resources.')
	}

})

server.listen(port)

console.log(`Server now accessible at localhost: ${port}`)